<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('mailbox', 'Mailboxes');
Route::resource('rules', 'RulesController');
Route::get('/api/check/rule', 'RulesController@sync');
Route::resource('jobs', 'CronJobController');
