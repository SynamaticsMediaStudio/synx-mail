<?php

namespace App\Http\Controllers;

use App\CronJob;
use App\Rules;
use Illuminate\Http\Request;

class CronJobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    $rules = Rules::where('status',1)->get();
        foreach ($rules as $rule){

        $hostname   = "{".$rule->Mailbox->hostname.":".$rule->Mailbox->port."/imap/".$rule->Mailbox->type."/norsh/novalidate-cert}".$rule->folder;
        $connection = @imap_open($hostname, $rule->Mailbox->username, $rule->Mailbox->password,NULL, 1, array('DISABLE_AUTHENTICATOR' => 'GSSAPI'));
        $emails     = imap_search($connection,'TEXT "'.$rule->data.'"');
        $read = 0;
        $moved = 0;

        if($emails) {
            echo "Found ".count($emails)." Messages <br> ";
            foreach($emails as $email_number) {         
                if($rule->type == "imap_mail_move"){
                    $imapresult = imap_mail_move($connection, $email_number, $rule->to);
                    if (!$imapresult) {
                        echo imap_last_error();
                    }
                    else{
                    }
                }
                elseif($rule->type == "imap_delete"){
                }
                elseif($rule->type == "imap_mail_move_read"){
                    $markread       = imap_setflag_full($connection, $email_number, "\\Seen");
                    $imapresult     = imap_mail_move($connection, $email_number, $rule->to);
                    
                    if (!$markread) {
                        echo imap_last_error()."<br>";
                    }
                    else{
                        $read++;
                    }
                    if (!$imapresult) {
                        echo imap_last_error()."<br>";
                    }
                    else{
                        $moved++;
                    }
                }
            }
            imap_expunge($connection);
            imap_close($connection);
        }
    }
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CronJob  $cronJob
     * @return \Illuminate\Http\Response
     */
    public function show(CronJob $cronJob)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CronJob  $cronJob
     * @return \Illuminate\Http\Response
     */
    public function edit(CronJob $cronJob)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CronJob  $cronJob
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CronJob $cronJob)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CronJob  $cronJob
     * @return \Illuminate\Http\Response
     */
    public function destroy(CronJob $cronJob)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CronJob  $cronJob
     * @return \Illuminate\Http\Response
     */
    public function sync()
    {
        $rules = Rules::where('status',1)->get();
        return $rules;
        foreach ($rules as $rule){

        $hostname   = "{".$rule->Mailbox->hostname.":".$rule->Mailbox->port."/imap/".$rule->Mailbox->type."/norsh/novalidate-cert}".$rule->folder;
        $connection = @imap_open($hostname, $rule->Mailbox->username, $rule->Mailbox->password,NULL, 1, array('DISABLE_AUTHENTICATOR' => 'GSSAPI'));
        $emails     = imap_search($connection,'TEXT "'.$rule->data.'"');
        if($emails) {        
            foreach($emails as $email_number) {         
                if($rule->type == "imap_mail_move"){
                    echo "Moving Mail $email_number From Folder<br>";
                    $imapresult = imap_mail_move($connection, $email_number, $rule->to);
                    if (!$imapresult) {
                        echo imap_last_error();
                    }
                    else{
                        echo "Completed<br>";
                    }
                }
                elseif($rule->type == "imap_delete"){
                    echo "Deleting From Folder";
                }
            }
            imap_expunge($connection);
            imap_close($connection);
        }
    }
    return "true";
    }
}
