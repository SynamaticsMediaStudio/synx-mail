<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mailbox;
class Mailboxes extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mailbox = Mailbox::all();
        return view("mailbox.index",["mailboxes"=>$mailbox]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("mailbox.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "hostname"=>"required",
            "port"=>"required",
            "type"=>"required",
            "username"=>"required",
            "password"=>"required",
        ]);
        $hostname = "{".$request->input('hostname').":".$request->input('port')."/imap/".$request->input('type')."/norsh/novalidate-cert}";
        $connection = @imap_open($hostname, $request->input('username'), $request->input('password'),NULL, 1, array('DISABLE_AUTHENTICATOR' => 'GSSAPI'));
        if($connection){
            $mailbox = new Mailbox();
            $mailbox->hostname = $request->input('hostname');
            $mailbox->port = $request->input('port');
            $mailbox->type = $request->input('type');
            $mailbox->username = $request->input('username');
            $mailbox->password = $request->input('password');
            $mailbox->save();
            return redirect()->route('mailbox.index')->with(["message"=>"New Mailbox Created and Connected Successfully"]);
        }
        else{
            return redirect()->back()->withInput($request->input())->withErrors(imap_errors());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mailbox = Mailbox::findorfail($id);
        $mailbox->host = "{".$mailbox->hostname.":".$mailbox->port."/imap/".$mailbox->type."/norsh/novalidate-cert}";
        $mailbox->connection = @imap_open($mailbox->host, $mailbox->username, $mailbox->password,NULL, 1, array('DISABLE_AUTHENTICATOR' => 'GSSAPI'));
        if(!$mailbox->connection){
            return imap_errors();
        }
        $folders  = collect(imap_list($mailbox->connection, $mailbox->host, '*'));
        $mailbox->folders = collect();
        foreach($folders as $mail) {
            $mailbox->folders->push(str_replace($mailbox->host, '', $mail));
        }
        return view("mailbox.show",["mailbox"=>$mailbox]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mailbox = Mailbox::findorfail($id);
        $mailbox->delete();
        return redirect()->route('mailbox.index')->with(["message"=>"Mailbox and Related Rules Deleted"]);
    }
}
