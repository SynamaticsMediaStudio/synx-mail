<?php

namespace App\Http\Controllers;

use App\Rules;
use App\Mailbox;
use Illuminate\Http\Request;

class RulesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rules = Rules::all();
        return view('rules.index',["rules"=>$rules]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mailboxes = Mailbox::all();
        foreach($mailboxes as $mailbox){
            $mailbox->host = "{".$mailbox->hostname.":".$mailbox->port."/imap/".$mailbox->type."/norsh/novalidate-cert}";
            $mailbox->connection = @imap_open($mailbox->host, $mailbox->username, $mailbox->password,NULL, 1, array('DISABLE_AUTHENTICATOR' => 'GSSAPI'));
            if(!$mailbox->connection){
                return imap_errors();
            }
            $folders  = collect(imap_list($mailbox->connection, $mailbox->host, '*'));
            $mailbox->folders = collect();
            foreach($folders as $mail) {
                $mailbox->folders->push(str_replace($mailbox->host, '', $mail));
            }
        }
        return view('rules.create',["mailboxes"=>$mailboxes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "folder"=>"required",
            "type"=>"required",
            "to"=>"nullable",
            "data"=>"required",
            "mailbox"=>"required",
        ]);
        
        $rule = new Rules();
        $rule->mailbox = $request->input('mailbox');
        $rule->folder = $request->input('folder');
        $rule->type = $request->input('type');
        $rule->to = ($request->input('to')) ? $request->input('to'):"";
        $rule->data = $request->input('data');
        $rule->status = "1";
        $rule->save();
        return redirect()->route('mailbox.show',[$request->input('mailbox')])->with(["message"=>"New Rule Added and Configured"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rules  $rules
     * @return \Illuminate\Http\Response
     */
    public function show(Rules $rules)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rules  $rules
     * @return \Illuminate\Http\Response
     */
    public function edit(Rules $rules)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rules  $rules
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rules $rules)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rules  $rules
     * @return \Illuminate\Http\Response
     */
    public function destroy($rules)
    {
       $rule = Rules::findorfail($rules);
       $mailbox = $rule->mailbox;
       $rule->delete();
       return redirect()->route('mailbox.show',[$mailbox])->with(["message"=>"Rule Removed Configured"]);
    }

    /**
     * Get Folders.
     *
     * @param  \App\Rules  $rules
     * @return \Illuminate\Http\Response
     */
    public function getfolders($id)
    {
       $rule = Rules::findorfail($id);
       
    }
}
