<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mailbox extends Model
{
    /**
     * Get all Rules.
     */
    public function Rules()
    {
        return $this->hasMany('App\Rules','mailbox','id');
    }
    public function delete(){
        $this->Rules()->delete();
        return parent::delete();
    }
}
