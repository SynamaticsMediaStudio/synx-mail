<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rules extends Model
{
    /**
     * Related to Mailbox.
     */
    public function Mailbox()
    {
        return $this->belongsTo('App\Mailbox','mailbox','id');
    }
}
