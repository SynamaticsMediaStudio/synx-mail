@extends('layouts.app')
@section('content')
<div class="container">
    <a href="{{route('mailbox.create')}}" class="btn btn-success float-right">New Mailbox</a>
    <h2>Mailboxes</h2>
    <table class="table">
        <thead class="table-dark">
            <tr>
                <th>Mailbox</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody class="table-bordered table-hover">
            @forelse ($mailboxes as $mailbox)
            <tr>
                <td>
                    <a href="{{route('mailbox.show',[$mailbox->id])}}">{{$mailbox->username}}</a>
                </td>
                <td>
                    <form action="{{route('mailbox.destroy',[$mailbox->id])}}" method="post">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
            @empty
            <tr>
                <td>No Mailboxes Found</td>
            </tr>
            @endforelse    
        </tbody>
    </table>
</div>
@endsection