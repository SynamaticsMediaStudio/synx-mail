@extends('layouts.app')
@section('content')
<div class="container">
    <a href="{{route('mailbox.index')}}" class="btn btn-primary btn-sm float-right">Back to Mailboxes</a>
    <h2>Add Mailbox</h2>
    <form action="{{route('mailbox.store')}}" method="post" class="form row">
        @csrf
        <div class="form-group col-sm-12 col-md-5">
            <label for="hostname">IMAP Hostname</label>
            <input type="text" class="form-control" name="hostname" id="hostname" value="{{old('hostname')}}">
        </div>
        <div class="form-group col-sm-12 col-md-4">
            <label for="port">IMAP Port</label>
            <input type="number" class="form-control" name="port" id="port" value="{{old('port')}}">
        </div>
        <div class="form-group col-sm-12 col-md-3">
            <label for="type">Protocol</label>
            <select class="form-control" name="type" id="type">
                <option @if(old('type') == "ssl") selected @endif value="ssl">SSL</option>
                <option @if(old('type') == "tls") selected @endif value="tls">TLS</option>
                <option @if(old('type') == "none") selected @endif value="none">None</option>
            </select>
        </div>
        <div class="form-group col-sm-12 col-md-6">
            <label for="username">IMAP Username</label>
            <input type="text" class="form-control" name="username" id="username" value="{{old('username')}}">
        </div>
        <div class="form-group col-sm-12 col-md-6">
            <label for="password">IMAP Password</label>
            <input type="password" class="form-control" name="password" id="password" value="{{old('password')}}">
        </div>
        <div class="form-group col-sm-12 col-md-6">
            <button type="submit" class="btn btn-submit">Save and Verify</button>
        </div>
    </form>
</div>
@endsection