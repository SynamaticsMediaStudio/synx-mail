@extends('layouts.app')
@section('content')
<div class="container">
    <a href="{{route('mailbox.index')}}" class="btn btn-primary btn-sm float-right">Back to Mailboxes</a>
    <a href="{{route('rules.create')}}" class="btn btn-default btn-sm float-right" data-toggle="modal" data-target="#add-rule-modal">Add New Rule</a>
    <h5>{{$mailbox->username}}</h5>
    <div class="py-4"></div>
    <h5>Folders Available</h5>
    <table class="table">
        <thead class="table-dark">
            <tr>
                <th>Mailbox Folder</th>
            </tr>
        </thead>
        <tbody class="table-bordered table-hover">
            @forelse ($mailbox->folders as $folder)
                <tr>
                    <th>{{$folder}}</th>
                </tr>
            @empty
                <th>No Folders Available</th>
            @endforelse
        </tbody>
    </table>
<!-- Button to Open the Modal -->
    <button type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#add-rule-modal">Add New Rule</button>
    <h5>Mailbox Rules</h5>
    <table class="table">
        <thead class="table-dark">
            <tr>
                <th>Rule</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody class="table-bordered table-hover">
            @forelse ($mailbox->Rules as $rules)
                <tr>
                    <th>{{__('default.'.$rules->type)}} {{$rules->folder}} @if($rules->to) to {{$rules->to}} @endif if {{$rules->data}} </th>
                    <th>
                        <div class="btn-group btn-group-sm">
                            <form action="{{route('rules.destroy',[$rules])}}" method="post">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                                <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                            </form>
                        </div>
                    </th>
                </tr>
            @empty
                <th>No Rules Available</th>
            @endforelse
        </tbody>
    </table>
</div>
      <!-- The Modal -->
    <div class="modal" id="add-rule-modal">
        <div class="modal-dialog b-0">
            <div class="modal-content">
                <!-- Modal body -->
                <div class="modal-body">
                        <h4 class="modal-title">Add New Rule</h4>
                <form action="{{route('rules.store')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="folder">For Folder</label>
                        <select name="folder" id="folder" class="form-control">
                            @forelse ($mailbox->folders as $folder)
                                <option value="{{$folder}}">{{$folder}}</option>
                            @empty
                                <option selected disabled>No Folders Available</option>
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="type">Action</label>
                        <select name="type" id="type" class="form-control" required>
                            <option selected disabled>Select an Action</option>
                            <option value="imap_mail_move">Move to Another Folder</option>
                            <option value="imap_mail_move_read">Move to Another Folder and Mark as Read</option>
                            <option value="imap_delete">Delete Mail</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="to">To Folder </label>
                        <select name="to" id="to" class="form-control">
                            @forelse ($mailbox->folders as $folder)
                                <option value="{{$folder}}">{{$folder}}</option>
                            @empty
                                <option selected disabled>No Folders Available</option>
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="data">Email ID / Keyword</label>
                        <input type="text" name="data" id="data" class="form-control">
                        <span class="help-text">Enter Email id, or Any Keyword that has the Following Text</span>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="mailbox" value="{{$mailbox->id}}">
                        <button type="submit" class="btn btn-success btn-block">Confirm and Save</button>
                    <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection