@extends('layouts.app')
@section('content')
<div class="container">
    <a href="{{route('rules.create')}}" class="btn btn-success float-right">New Rule</a>
    <h2>Rules</h2>
    <table class="table">
        <thead class="table-dark">
            <tr>
                <th>Mailbox</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody class="table-bordered table-hover">
            @forelse ($rules as $rule)
            <tr>
                <td>
                    {{__('default.'.$rule->type)}} <a href="{{route('mailbox.show',[$rule->Mailbox->id])}}">{{$rule->Mailbox->username}}</a>/{{$rule->folder}} @if($rule->to) to {{$rule->to}} @endif if {{$rule->data}}
                </td>
                <td>
                    <form action="{{route('rules.destroy',[$rule->id])}}" method="post">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
            @empty
            <tr>
                <td>No Rules Found</td>
            </tr>
            @endforelse    
        </tbody>
    </table>
</div>
@endsection