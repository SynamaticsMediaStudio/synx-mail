@extends('layouts.app')
@section('content')
<div class="container">
        <form action="{{route('rules.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="folder">For Folder</label>
                    <select name="folder" id="folder" class="form-control">
                        @forelse ($mailboxes as $mailbox)
                            @forelse ($mailbox->folders as $folder)
                                <option value="{{$folder}}">{{$mailbox->username}}/{{$folder}}</option>
                            @empty
                            <option selected disabled>No Folders Available</option>
                            @endforelse
                        @empty
                        @endforelse
                    </select>
                </div>
                <div class="form-group">
                    <label for="type">Action</label>
                    <select name="type" id="type" class="form-control" required>
                        <option selected disabled>Select an Action</option>
                        <option value="imap_mail_move">Move to Another Folder</option>
                        <option value="imap_mail_move_read">Move to Another Folder and Mark as Read</option>
                        <option value="imap_delete">Delete Mail</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="to">To Folder </label>
                    <select name="to" id="to" class="form-control">
                        @forelse ($mailboxes as $mailbox)
                            @forelse ($mailbox->folders as $folder)
                                <option value="{{$folder}}">{{$mailbox->username}}/{{$folder}}</option>
                            @empty
                            <option selected disabled>No Folders Available</option>
                            @endforelse
                        @empty
                        @endforelse
                    </select>
                </div>
                <div class="form-group">
                    <label for="data">Email ID / Keyword</label>
                    <input type="text" name="data" id="data" class="form-control">
                    <span class="help-text">Enter Email id, or Any Keyword that has the Following Text</span>
                </div>
                <div class="form-group">
                    <input type="hidden" name="mailbox" value="{{$mailbox->id}}">
                    <button type="submit" class="btn btn-success btn-block">Confirm and Save</button>
                <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Cancel</button>
                </div>
            </form>
</div>
@endsection